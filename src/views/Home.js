import * as React from 'react'
import JokeWrapper from '../components/JokeWrapper'
import { useQueryClient } from 'react-query'
import './Home.css'
function Home() {
	const queryClient = useQueryClient()

	const onGenerate = () => {
		queryClient.invalidateQueries('joke')
	}

	return (
		<div className='app'>
			<h1>Generator šala</h1>
			<div className='jokeWrap'>
				<JokeWrapper />
			</div>
			<button onClick={onGenerate} className='btn'>
				Nova šala
			</button>
		</div>
	)
}

export default Home
