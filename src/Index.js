import {  QueryClient,QueryClientProvider } from 'react-query';
import Home from './views/Home';
import * as React from 'react'
import { Toaster } from 'react-hot-toast';


const Index = () => {
    const queryClient = new QueryClient({
        defaultOptions: {
            queries: {
                refetchOnWindowFocus: false
            }
        }
    })


    return <QueryClientProvider client={queryClient}>
        <Toaster position='bottom-center'/>
        <Home/>
    </QueryClientProvider>


}

export default Index